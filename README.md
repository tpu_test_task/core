# Тестовое задание

## Запуск
```shell
cp ./.env.sample ./.env
# Отредактируйте файл конфигурации
docker-compose up -d --build
docker-compose logs -f
```

## Микросервисы
### Auth
Использует порт 3000  
Эндпоинты:
*  `POST /api/signup` 
* `POST /api/signin` 
* `GET /api/user` !

### ToDo
Использует порт 4000  
Эндпоинты:
* `POST /tasks/create` !
* `DELETE /tasks/delete` !
* `GET /tasks/get` !

Эндпоинты, отмеченные символом `!`, требует Bearer токен.
